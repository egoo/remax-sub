/**
 * Maximize window client side for share popup center
 *
 * @author Faizal Pribadi
 */
;(function($){

  $.fn.sharePopup= function (e, intWidth, intHeight, blnResize) {
    e.preventDefault();
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

    width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    intWidth = intWidth || '900';
    intHeight = intHeight || '500';
    strResize = (blnResize ? 'yes' : 'no');

    var left = ((width / 2) - (intWidth / 2)) + dualScreenLeft;
    var top = ((height / 2) - (intHeight / 2)) + dualScreenTop;
    var strTitle = ((typeof this.attr('title') !== 'undefined') ? this.attr('title') : 'Social Share'),
        strParam = 'width=' + intWidth + ',height=' + intHeight + ',resizable=' + strResize + ',top=' + top + ',left=' + left,
        objWindow = window.open(this.attr('href'), strTitle, strParam).focus();
  }

  $(document).ready(function ($) {
    $('.customer.share').on("click", function(e) {
      $(this).sharePopup(e);
    });
  });

}(jQuery));
